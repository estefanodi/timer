# Timer App v1

Simple timer desktop app built with Python3 and PyQt5 module, designed with Qt Creator.

Next version adding the possibility to save times using sqlite.

# How to run it

1) Clone the repo

2) cd timer

3) python3 Timer_Window.py

<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1625232964/Screenshot_from_2021-07-02_15-29-42.png" alt="splash screen" width='500px'>
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import QtCore, QtGui
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import *

from Ui_Timer import Ui_Timer
from data import data_hours, data_minutes, data_seconds

hours = data_hours
minutes = data_minutes
seconds = data_seconds


def write_file(h, m, s):
    f = open("data.py", "a")
    f.truncate(0)
    f.write(f'data_hours={h}\n')
    f.write(f'data_minutes={m}\n')
    f.write(f'data_seconds={s}\n')
    f.write(f'projects={projects}')
    f.close()


class Timer_Window:
    def __init__(self):
        self.main_win = QMainWindow()
        self.ui = Ui_Timer()
        self.ui.setupUi(self.main_win)
        self.timer = QtCore.QTimer()
        self.icon = QtGui.QIcon()
        self.timer.timeout.connect(lambda: self.increment())
        self.ui.play_button.clicked.connect(self.play)
        self.ui.pause_button.clicked.connect(self.pause)
        self.ui.reset_button.clicked.connect(self.reset)
        # * INITIAL LABELS
        self.ui.hours_label.setText(self.get_number(hours, 'hours'))
        self.ui.minutes_label.setText(self.get_number(minutes, 'minutes'))
        self.ui.seconds_label.setText(self.get_number(seconds, 'seconds'))

    def show(self):
        self.main_win.show()

    def get_number(self, num, tp):
        if num == 60 and tp != 'hours':
            return '00'
        if num < 10:
            return '0' + str(num)
        else:
            return str(num)

    def toggle_play_button(self, status):
        self.icon.addPixmap(QtGui.QPixmap("assets/play3.png" if status == 'active' else "assets/play1.png"),
                            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.play_button.setIconSize(QtCore.QSize(
            64, 64) if status == 'active' else QtCore.QSize(55, 55))
        self.ui.play_button.setIcon(self.icon)

    def toggle_pause_button(self, status):
        self.icon.addPixmap(QtGui.QPixmap("assets/pause3.png" if status == 'active' else "assets/pause1.png"),
                            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.pause_button.setIconSize(QtCore.QSize(
            64, 64) if status == 'active' else QtCore.QSize(55, 55))
        self.ui.pause_button.setIcon(self.icon)

    def play(self):
        self.timer.start(1000)
        self.toggle_play_button('active')
        self.toggle_pause_button('inactive')

    def pause(self):
        global minutes, seconds, hours
        self.timer.stop()
        if hours == 0 and minutes == 0 and seconds == 0:
            return
        self.toggle_play_button('inactive')
        self.toggle_pause_button('active')

    def reset(self):
        global minutes, seconds, hours
        self.ui.hours_label.setText(self.get_number(0, 'hours'))
        self.ui.minutes_label.setText(self.get_number(0, 'minutes'))
        self.ui.seconds_label.setText(self.get_number(0, 'seconds'))
        self.toggle_play_button('inactive')
        self.toggle_pause_button('inactive')
        write_file(0, 0, 0)
        seconds = 0
        minutes = 0
        hours = 0
        self.timer.stop()

    def increment(self):
        global minutes, seconds, hours
        seconds += 1
        self.ui.seconds_label.setText(self.get_number(seconds, 'seconds'))
        if seconds == 60:
            seconds = 0
            minutes += 1
            self.ui.minutes_label.setText(self.get_number(minutes, 'minutes'))
        if minutes == 60:
            minutes = 0
            hours += 1
            self.ui.hours_label.setText(self.get_number(hours, 'minutes'))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.aboutToQuit.connect(lambda: write_file(hours, minutes, seconds))
    app.setWindowIcon(QIcon('assets/timer.png'))
    window = Timer_Window()
    window.show()
    sys.exit(app.exec_())

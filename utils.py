def find(list,key,value):
    return next((item for item in list if item[key] == value), None)

def find_index(list,key,value):
    return next((i for i, item in enumerate(list) if item[key] == value), -1)